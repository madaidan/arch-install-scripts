#!/bin/bash -e

#    Arch Install Scripts
#    Copyright (C) 2019  madaidan
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

greeting() {
  # Greeting.
  echo "Hello. This will guide you through the post-install of Arch Linux."
  read -r -p "Do you want to start this now? Entering 'n' will shutdown the computer. " start
  if [ "${start}" = "n" ]; then
    shutdown 0
  fi
}

setup_ethernet() {
  # Setup ethernet.
  read -r -p "Do you want to use ethernet? (y/n) " ethernet
  if [ "${ethernet}" = "y" ]; then
    # Enables systemd dhcpcd service.
    systemctl enable dhcpcd
  fi
}

setup_wifi() {
  # Setup WiFi.
  read -r -p "Do you want to use WiFi? (y/n) " wifi
  if [ "${wifi}" = "y" ]; then
    # Details needed to create connection.
    read -r -p "What is it called? " wifi_name
    read -r -p "What is the password? " wifi_password
    read -r -p "What is your wifi interface? " wifi_interface

    # Copy netctl example profile.
    cp /etc/netctl/examples/wireless-wpa /etc/netctl/"${wifi_name}"

    # Insert needed details.
    sed -i "s/MyNetwork/${wifi_name}/" /etc/netctl/"${wifi_name}"  #(Change interface, ESSID, Key [Password])
    sed -i "s/WirelessKey/${wifi_password}/" /etc/netctl/"${wifi_name}"
    sed -i "s/wlan0/${wifi_interface}/" /etc/netctl/"${wifi_name}"

    # Start the WiFi connection.
    netctl start "${wifi_name}"
  fi
}

get_locale() {
  # List supported locales.
  echo "Supported locales:"
  echo ""
  awk '{print $1}' /etc/locale.gen | sed -e's/#//g' | sed -e 's/@.*//g' | sed -e 's/\..*//g' | awk '!x[$0]++' | xargs | sed -e 's/ /, /g'
  echo ""

  read -r -p "Which locale do you want? " locale
  locale_warning="0"

  # Run again if the user enters an invalid locale.
  for i in $(awk '{print $1}' /etc/locale.gen | sed -e's/#//g' | sed -e 's/@.*//g' | sed -e 's/\..*//g' | awk '!x[$0]++')
  do
    if [ "${locale}" = "${i}" ]; then
      locale_warning="1"
      break
    fi
  done

  if ! [ "${locale_warning}" = "1" ]; then
    echo "ERROR: That is not a supported locale."
    echo ""
    get_locale
  fi
}

get_keymap() {
  # List supported keymaps.
  echo "Supported keymaps:"
  echo ""
  find /usr/share/kbd/keymaps/ -type f | awk -F"/" '{print $NF}' | sed -e 's/.map.gz//g' | xargs | sed -e 's/ /, /g'
  echo ""

  read -r -p "Which keymap do you want? " keymap
  keymap_warning="0"

  # Run again if the user enters an invalid keymap.
  for i in $(find /usr/share/kbd/keymaps/ -type f | awk -F"/" '{print $NF}' | sed -e 's/.map.gz//g')
  do
    if [ "${keymap}" = "${i}" ]; then
      keymap_warning="1"
      break
    fi
  done

  if ! [ "${keymap_warning}" = "1" ]; then
    echo "ERROR: That is not a supported keymap."
    echo ""
    get_keymap
  fi
}

set_locale_and_keymap() {
  # Set locale.
  get_locale
  localectl set-locale LANG="${locale}.UTF-8"

  # Set keymap.
  get_keymap
  localectl set-keymap --no-convert "${keymap}"
}

get_kernel() {
  echo ""

  # Say if already installed.
  if pacman -Qq linux-hardened &>/dev/null; then
    echo "1) linux-hardened - ALREADY INSTALLED"
  else
    echo "1) linux-hardened"
  fi

  if pacman -Qq linux-lts &>/dev/null; then
    echo "2) linux-lts - ALREADY INSTALLED"
  else
    echo "2) linux-lts"
  fi
  echo "3) None"
  echo ""

  read -r -p "Which kernel do you want to install? " which_kernel

  # Run again if the user enters an invalid kernel.
  if ! [ "${which_kernel}" = "1" ] && ! [ "${which_kernel}" = "2" ] && ! [ "${which_kernel}" = "3" ]; then
    echo "ERROR: That is not a supported kernel."
    echo ""
    get_kernel
  fi
}

install_extra_kernels() {
  # Install extra kernels.
  if ! [ "${another_kernel}" = "y" ]; then
    read -r -p "Do you want to install an extra kernel? (y/n) " install_kernels
  fi

  if [ "${install_kernels}" = "y" ]; then
    # Select which kernel to install.
    get_kernel
    if [ "${which_kernel}" = "1" ]; then
      # Install linux-hardened.
      pacman -S --noconfirm -q linux-hardened linux-hardened-headers

      # Generate initcpio.
      mkinitcpio -p linux-hardened
    fi

    if [ "${which_kernel}" = "2" ]; then
      # Install linux-lts.
      pacman -S --noconfirm -q linux-lts linux-lts-headers

      # Generate initcpio.
      mkinitcpio -p linux-lts
    fi

    if ! [ "${which_kernel}" = "3" ]; then
      # Generate new GRUB configuration.
      grub-mkconfig -o /boot/grub/grub.cfg

      # Install another kernel.
      read -r -p "Install another kernel? (y/n) " another_kernel
      if [ "${another_kernel}" = "y" ]; then
        install_extra_kernels
      fi
    fi
  fi
}

set_root_password() {
  # Set root password.
  read -r -p "Do you want to set a root password? (y/n) " rootpass
  if [ "${rootpass}" = "y" ]; then
    passwd
  fi
}

create_swap() {
  # Create swap file.
  read -r -p "Would you like a swap file? (y/n) " swapfile
  if [ "${swapfile}" = "y" ]; then
    read -r -p "How big do you want it? Enter 'G' or 'M' e.g '2G' " swapsize
    # Create /swapfile.
    fallocate -l "${swapsize}" /swapfile

    # Set the swapfile's mode to 600/
    chmod 600 /swapfile

    # Use /swapfile as swap.
    mkswap /swapfile

    # Add /swapfile to fstab.
    echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab >/dev/null
  fi
}

install_packages() {
  # Install networkmanager.
  read -r -p "Do you want to install networkmanager? (y/n) " netmn
  if [ "${netmn}" = "y" ]; then
    pacman -S --noconfirm -q networkmanager network-manager-applet
  fi

  # Install tools needed for WiFi.
  read -r -p "Do you want to install tools needed for WiFi? (y/n) " install_wifi_tools
  if [ "${install_wifi_tools}" = "y" ]; then
    pacman -S --noconfirm -q wireless_tools wpa_supplicant
  fi

  # Install xorg-server and xorg-xinit.
  read -r -p "Do you want to install xorg-server and xorg-xinit? (y/n) " install_xorg
  if [ "${install_xorg}" = "y" ]; then
    (echo   ) | pacman -S --noconfirm -q xorg-server xorg-xinit mesa
  fi

  # Add multilib repo.
  read -r -p "Do you want to add the multilib repo to pacman.conf? This allows you to run 32-bit programs. (y/n) " multilib
  if [ "${multilib}" = "y" ]; then
    sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf

    # Updates mirrorlists.
    pacman -Syyy
  fi

  # Add touchpad support.
  read -r -p "Do you want touchpad support? (y/n) " touchpad
  if [ "${touchpad}" = "y" ]; then
    pacman -S --noconfirm -q xf86-input-libinput
  fi

  # Install sudo.
  read -r -p "Do you want to install and configure sudo? (y/n) " sudo
  if [ "${sudo}" = "y" ]; then
    # Installs sudo.
    pacman -S --noconfirm -q sudo

    # Allows members of the wheel group to use sudo.
    sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
  fi
}

get_drivers() {
  echo ""

  # Say if already installed.
  if pacman -Qq xf86-video-intel &>/dev/null; then
    echo "1) Intel drivers - ALREADY INSTALLED"
  else
    echo "1) Intel drivers"
  fi

  if pacman -Qq virtualbox-guest-dkms &>/dev/null; then
    echo "2) Virtualbox drivers - ALREADY INSTALLED"
  elif pacman -Qq virtualbox-guest-modules-arch &>/dev/null; then
    echo "2) Virtualbox drivers - ALREADY INSTALLED"
  else
    echo "2) Virtualbox drivers"
  fi
  echo "3) None"
  echo ""

  read -r -p "Which driver do you want to install? " which_driver

  # Run again if the user enters an invalid option.
  if ! [ "${which_driver}" = "1" ] && ! [ "${which_driver}" = "2" ] && ! [ "${which_driver}" = "3" ]; then
    echo "ERROR: That is not a supported option."
    echo ""
    get_drivers
  fi
}

install_drivers() {
  # Install Intel or virtualbox drivers.
  if ! [ "${another_driver}" = "y" ]; then
    read -r -p "Do you want to install extra drivers? (y/n) " install_extra_drivers
  fi

  if [ "${install_extra_drivers}" = "y" ]; then
    get_drivers

    # Install Intel drivers.
    if [ "${which_driver}" = "1" ]; then
      pacman -S --noconfirm -q xf86-video-intel

      # Install multilib Intel drivers.
      if [ "${multilib}" = "y" ]; then
        # Install multilib Intel drivers.
        pacman -S --noconfirm -q lib32-intel-dri lib32-mesa lib32-libgl
      fi
    fi

    # Install Virtualbox drivers.
    if [ "${which_driver}" = "2" ]; then
      read -r -p "Are you using the default kernel? (y/n) " default_kernel
      read -r -p "Do you want X11 support? (y/n) " xsupport

      if [ "${default_kernel}" = "y" ]; then
        if [ "${xsupport}" = "y" ]; then
          # Install default kernel virtualbox drivers with X11 support.
          (echo 2 ) | pacman -S --noconfirm -q virtualbox-guest-utils
        elif [ "${xsupport}" = "n" ]; then
          # Install default kernel virtualbox drivers without X11 support.
          (echo 2 ) | pacman -S --noconfirm -q virtualbox-guest-utils-nox
        fi
      elif [ "${default_kernel}" = "n" ]; then
        if [ "${xsupport}" = "y" ]; then
          # Install non-default kernel virtualbox drivers with X11 support.
          (echo 1 ) | pacman -S --noconfirm -q virtualbox-guest-utils
        elif [ "${xsupport}" = "n" ]; then
          # Install non-default kernel virtualbox drivers without X11 support.
          (echo 1 ) | pacman -S --noconfirm -q virtualbox-guest-utils-nox
        fi
      fi
    fi

    if ! [ "${which_driver}" = "3" ]; then
      # Install another driver.
      read -r -p "Install another driver? (y/n) " another_driver
      if [ "${another_driver}" = "y" ]; then
        install_drivers
      fi
    fi
  fi
}

configure_system() {
  # Add user.
  read -r -p "Do you want to add a user? (y/n) " user
  if [ "${user}" = "y" ]; then
    read -r -p "What do you want to name it? " username

    # Create new user in the wheel group with a home directory and set its default shell to bash.
    useradd -m -G wheel -s /bin/bash "${username}"
    read -r -p "Do you want to set a password for your user? (y/n) " passwduser
    if [ "${passwduser}" = "y" ]; then
      # Change the user's password.
      passwd "${username}"
    fi
  fi

  # Change the hostname.
  read -r -p "Do you want to change the hostname? (y/n) " change_hostname
  if [ "${change_hostname}" = "y" ]; then
    read -r -p "What is the new hostname? " new_hostname

    # Set the hostname.
    hostnamectl set-hostname "${new_hostname}"
  fi
}

get_desktop() {
  echo ""

  # Say if already installed.
  if pacman -Qq xfce4-session &>/dev/null; then
    echo "1) XFCE - ALREADY INSTALLED"
  else
    echo "1) XFCE"
  fi

  if pacman -Qq gnome-session &>/dev/null; then
    echo "2) GNOME - ALREADY INSTALLED"
  else
    echo "2) GNOME"
  fi

  if pacman -Qq plasma-desktop &>/dev/null; then
    echo "3) KDE - ALREADY INSTALLED"
  else
    echo "3) KDE"
  fi

  if pacman -Qq mate-desktop &>/dev/null; then
    echo "4) MATE - ALREADY INSTALLED"
  else
    echo "4) MATE"
  fi

  echo "5) None"
  echo ""

  read -r -p "Which desktop environment do you want to install? " which_desktop_environment

  # Run again if the user enters an invalid option.
  if ! [ "${which_desktop_environment}" = "1" ] && ! [ "${which_desktop_environment}" = "2" ] && ! [ "${which_desktop_environment}" = "3" ] && ! [ "${which_desktop_environment}" = "4" ] && ! [ "${which_desktop_environment}" = "5" ]; then
    echo "ERROR: That is not a supported desktop environment."
    echo ""
    get_desktop
  fi
}


install_desktop_environment() {
  # Install a DE.
  if ! [ "${another_desktop_environment}" = "y" ]; then
    read -r -p "Do you want to install a desktop environment? (y/n) " install_de
  fi

  if [ "${install_de}" = "y" ] ; then
    # Install xorg-server and xorg-xinit. These are needed to start the DEs.
    if ! [ "${install_xorg}" = "y" ]; then
      (echo   ) | pacman -S --noconfirm -q xorg-server xorg-xinit mesa
    fi

    get_desktop

    if ! [ "${another_desktop_environment}" = "y" ]; then
      read -r -p "Do you want to create a xinitrc for your desktop environment? (This is needed to start it with 'startx') (y/n) " create_xinitrc
      if [ "${create_xinitrc}" = "y" ]; then
        if [ "${user}" = "y" ]; then
          # Path to xinitrc.
          xinitrc_file="/home/${username}/.xinitrc"

          # Copy xinitrc to user's home directory.
          cp /etc/X11/xinit/xinitrc "${xinitrc_file}"
        elif [ "${user}" = "n" ]; then
          # Path to xinitrc.
          xinitrc_file="/etc/X11/xinit/xinitrc"

          # Create a backup of the xinitrc.
         cp /etc/X11/xinit/xinitrc /etc/X11/xinit/xinitrc.backup
        fi

        # Removes unnessecary lines from xinitrc.
        for i in {1..5}
        do
          sed -i '51d' ${xinitrc_file}
        done
      fi
    fi

    # Installs XFCE.
    if [ "${which_desktop_environment}" = "1" ] ; then
      pacman -S --noconfirm -q xfce4

      # Edit the xinitrc for XFCE.
      if [ "${create_xinitrc}" = "y" ]; then
        echo "exec startxfce4" | tee -a "${xinitrc_file}" >/dev/null
      fi
    fi

    # Installs GNOME.
    if [ "${which_desktop_environment}" = "2" ] ; then
      pacman -S --noconfirm -q gnome

      # Edit the xinitrc for GNOME.
      if [ "${create_xinitrc}" = "y" ]; then
        echo "export GDK_BACKEND=x11" | tee -a "${xinitrc_file}" >/dev/null
        echo "exec gnome-session" | tee -a "${xinitrc_file}" >/dev/null
      fi
    fi

    # Installs KDE.
    if [ "${which_desktop_environment}" = "3" ] ; then
      pacman -S --noconfirm -q plasma-desktop

      # Edit the xinitrc for KDE.
      if [ "${create_xinitrc}" = "y" ]; then
        echo "exec startkde" | tee -a "${xinitrc_file}" >/dev/null
      fi
    fi

    # Installs MATE.
    if [ "${which_desktop_environment}" = "4" ] ; then
      sudo pacman -S --noconfirm -q mate

      # Edit the xinitrc for MATE.
      if [ "${create_xinitrc}" = "y" ]; then
        echo "exec mate-session" | tee -a ${xinitrc_file} >/dev/null
      fi
    fi

    if ! [ "${which_desktop_environment}" = "5" ]; then
      # Install another driver.
      read -r -p "Install another desktop environment? (y/n) " another_desktop_environment
      if [ "${another_desktop_environment}" = "y" ]; then
        install_desktop_environment
      fi
    fi
  fi
}

get_de_extras() {
  echo ""

  # Say if already installed.
  if pacman -Qq xfce4-goodies &>/dev/null; then
    echo "1) xfce4-goodies - ALREADY INSTALLED"
  else
    echo "1) xfce4-goodies"
  fi

  if pacman -Qq gnome-session &>/dev/null; then
    echo "2) gnome-extra - ALREADY INSTALLED"
  else
    echo "2) gnome-extra"
  fi

  if pacman -Qq plasma-desktop &>/dev/null; then
    echo "3) kde-applications - ALREADY INSTALLED"
  else
    echo "3) kde-applications"
  fi

  if pacman -Qq mate-desktop &>/dev/null; then
    echo "4) mate-extra - ALREADY INSTALLED"
  else
    echo "4) mate-extra"
  fi

  echo "5) None"
  echo ""

  read -r -p "Which extra desktop environment programs do you want to install? " which_de_extras

  # Run again if the user enters an invalid option.
  if ! [ "${which_de_extras}" = "1" ] && ! [ "${which_de_extras}" = "2" ] && ! [ "${which_de_extras}" = "3" ] && ! [ "${which_de_extras}" = "4" ] && ! [ "${which_de_extras}" = "5" ]; then
    echo "ERROR: That is not a supported option."
    echo ""
    get_de_extras
  fi
}

install_extra_de_programs() {
  if ! [ "${more_de_programs}" = "y" ]; then
    read -r -p "Do you want to install extra desktop environment programs? (y/n) " install_de_extras
  fi

  if [ "${install_de_extras}" = "y" ]; then
    get_de_extras

    # Installs extra XFCE programs.
    if [ "${which_de_extras}" = "1" ] ; then
      pacman -S --noconfirm -q xfce4-goodies
    fi

    # Installs extra GNOME programs.
    if [ "${which_de_extras}" = "2" ] ; then
      pacman -S --noconfirm -q gnome-extra
    fi

    # Installs extra KDE programs.
    if [ "${which_de_extras}" = "3" ] ; then
      sudo pacman -S --noconfirm -q kde-applications
    fi

    # Installs extra MATE programs.
    if [ "${mate-which_de_extras}" = "4" ] ; then
      sudo pacman -S --noconfirm -q mate-extra
    fi

    if ! [ "${which_desktop_environment}" = "5" ]; then
      # Install more desktop environment programs..
      read -r -p "Install more extra desktop environment programs? (y/n) " more_de_programs
      if [ "${more_de_programs}" = "y" ]; then
        install_extra_de_programs
      fi
    fi
  fi
}

configure_x11() {
    # Auto startx at login.
    if [ "${create_xinitrc}" = "y" ]; then
      read -r -p "Do you want to automatically start your DE at login? (y/n) " autostartx

      # Use user's bashrc if a user was created.
      if [ "${user}" = "y" ]; then
        # Create bashrc if not already.
        if ! [ -f "/home/${username}/.bashrc" ]; then
          cp /etc/skel/.bashrc "/home/${username}/.bashrc"
          chown "${username}" "/home/${username}/.bashrc"
          chmod 644 "/home/${username}/.bashrc"
        fi

        # Set the variable to the user's bashrc.
        bashrc_file="/home/${username}/.bashrc"
      else
        bashrc_file="/etc/bash.bashrc"
      fi

      if [ "${autostartx}" = "y" ]; then
        echo '''
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi ''' | tee -a "${bashrc_file}" >/dev/null
      fi
  fi
}

install_extra_programs() {
  # Install Firefox.
  read -r -p "Do you want to install Firefox? (y/n) " firefox
  if [ "${firefox}" = "y" ] ; then
    pacman -S --noconfirm -q firefox >/dev/null
  fi

  # Install ProtonVPN.
  read -r -p "Do you want to install the ProtonVPN client? (y/n)  " pvpn
  if [ "${pvpn}" = "y" ] ; then
    # Install needed packages.
    pacman -S --noconfirm -q openvpn python dialog wget

    # Download ProtonVPN client.
    wget -O --https-only protonvpn-cli.sh https://raw.githubusercontent.com/ProtonVPN/protonvpn-cli/master/protonvpn-cli.sh

    # Make it executable.
    chmod +x protonvpn-cli.sh

    # Install it.
    ./protonvpn-cli.sh --install

    # Initialize profile.
    pvpn -init
    echo "Run 'pvpn -f' to connect to the fastest server or run 'pvpn -c' to choose which server to connect to."
  fi

  # Install yay.
  read -r -p "Do you want to install yay (an AUR helper)? (y/n) " install_yay
  if [ "${install_yay}" = "y" ]; then
    # Install git.
    pacman -S --noconfirm -q git >/dev/null

    # Install base-devel.
    pacman -S --noconfirm -q base-devel

    # Create a user for installing yay as yay cannot be run as root.
    useradd -m --home-dir /home/yay -s /bin/bash yay

    # Allow the yay user to run sudo without a password (needed for using makepkg without entering a password).
    echo "yay ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers >/dev/null

    # Clone yay.
    git clone https://aur.archlinux.org/yay.git /home/yay/yay
    chown yay -R /home/yay
    continue_yay_installation="y"

    # Verify PKGBUILD.
    read -r -p "Do you want to verify the yay PKGBUILD? (y/n) " verify_yay_pkgbuild
    if [ "${verify_yay_pkgbuild}" = "y" ]; then
      less /home/yay/yay/PKGBUILD

      # Allow the user to stop yay from being installed.
      read -r -p "Continue installing yay? (y/n) " continue_yay_installation
    fi

    if [ "${continue_yay_installation}" = "y" ]; then
      # Install yay as the yay user.
      sudo -u yay sh -c "cd /home/yay/yay && makepkg --noconfirm -si"
    else
      echo "Yay has not been installed."
    fi

    # Delete yay user.
    userdel -rf yay &>/dev/null
    sed -i 's/yay ALL=(ALL) NOPASSWD: ALL//' /etc/sudoers
  fi
}

greeting
setup_ethernet
setup_wifi
set_locale_and_keymap
install_extra_kernels
set_root_password
create_swap
install_packages
install_drivers
configure_system
install_desktop_environment
install_extra_de_programs
configure_x11
install_extra_programs

# Reboot.
read -r -p "Reboot now? (y/n) " reboot
if [ "${reboot}" = "y" ] ; then
  reboot
fi
