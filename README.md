# Arch Install Scripts

These are install scripts for Arch Linux.
There are 2 scripts. "archinstall.sh" is for the base installation and "postarchinstall.sh" is for after the inital installation.

The scripts currently only have support for GRUB and Xorg.

## USING THE SCRIPTS:

To use the install script, run `wget https://gitlab.com/madaidan/arch-install-scripts/raw/master/archinstall.sh && sh archinstall.sh` in the archiso.

To use the post install script, reboot after installing the system and run `wget https://gitlab.com/madaidan/arch-install-scripts/raw/master/postarchinstall.sh && sh postarchinstall.sh`.